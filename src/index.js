const express = require('express');
const deltaRouter = require('./routers/delta');

const app = express();
const port = process.env.PORT || 3009;

app.use(express.json());

app.use(deltaRouter);

app.listen(port, () => {
    console.log('Server listening on ' +  port);
});